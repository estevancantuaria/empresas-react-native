import 'react-native-gesture-handler';
import React,{useEffect} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {StatusBar } from 'react-native';
import {Provider} from 'react-redux';
import store from './src/store'
import Routes from './src/routes/index';
import { useSelector } from 'react-redux';

export default function App() {
  return (
        <Provider store={store}>
            <NavigationContainer>
                <StatusBar backgroundColor="#131313" barStyle="light-content"/>
                <Routes/>
              </NavigationContainer>
        </Provider>
    );
}