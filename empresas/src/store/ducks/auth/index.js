import { createAction, createReducer } from '@reduxjs/toolkit';

const INITIAL_STATE = {
    isAuthenticated: false,
    isLoading: false
}

export const login = createAction('LOGIN')
export const logout = createAction('LOGOUT')
export const loading = createAction('LOADING')
export const stopLoading = createAction('STOP_LOADING')

export default createReducer(INITIAL_STATE, {
    [login.type]: (state, action) => ({ ...state, isAuthenticated: true }),
    [stopLoading.type]: (state, action) => ({ ...state, isLoading: false }),
    [loading.type]: (state, action) => ({ ...state, isLoading: true }),
    [logout.type]: (state, action) => ({ ...state, isAuthenticated: false })
})