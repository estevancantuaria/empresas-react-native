import { createAction, createReducer } from "@reduxjs/toolkit";

const INITIAL_STATE = {}

export const listEnterprises = createAction('LIST_ENTERPRISES')

export default createReducer(INITIAL_STATE, {
    [listEnterprises.type]: (state, action) => ({ ...action.payload })
})