import { createAction, createReducer } from "@reduxjs/toolkit";

const INITIAL_STATE = {
    enterprises: {},
    isLoading: false
}

export const filterEnterprises = createAction('FILTER_ENTERPRISES')
export const loadingFilter = createAction('LOADING_FILTER')
export const stopLoadingFilter = createAction('STOP_LOADING_FILTER')

export default createReducer(INITIAL_STATE, {
    [filterEnterprises.type]: (state, action) => ({ ...state, enterprises: action.payload }),
    [loadingFilter.type]: (state, action) => ({ ...state, isLoading: true }),
    [stopLoadingFilter.type]: (state, action) => ({ ...state, isLoading: false }),
})