import { login, logout, loading, stopLoading } from "../ducks/auth";
import { listEnterprises } from '../ducks/enterprises'
import { filterEnterprises, loadingFilter, stopLoadingFilter } from '../ducks/filteredEnterprises'
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';


export const authLogin = (email, password) => {

    const options = {
        headers: { 'Content-Type': 'application/json' }
    }

    const user = {
        email: email,
        password: password
    }

    return dispatch => {
        dispatch(loading())
        axios.post('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', user, options)
            .then(res => {
                if (res.headers) {
                    storageUser(res.headers).then(() => {
                        dispatch(login())
                    }).catch((error) => {
                        console.log(error);
                    })
                }
            })
            .catch((error) => {
                alert("Invalid credentials");
                dispatch(stopLoading())
                console.log(error);
            })
    }
}


export const storageUser = async (data) => {
    await AsyncStorage.setItem('Auth_user', JSON.stringify(data));
}

export const signOut = () => {
    return async dispatch => {
        await AsyncStorage.clear()
        dispatch(logout())
    }
}

export const getHeadersFromStorage = async () => {

    let storageUser = {}

    await AsyncStorage.getItem('Auth_user').then(res => {
        storageUser = JSON.parse(res)
    }).catch(error => {
        console.log(error);
    })

    return {
        headers: {
            'Content-Type': 'application/json',
            'access-token': storageUser['access-token'],
            'client': storageUser['client'],
            'uid': storageUser['uid']
        }
    }
}

export const enterprisesList = () => {
    return async dispatch => {
        dispatch(loading())
        let headers = await getHeadersFromStorage()
        axios.get('https://empresas.ioasys.com.br/api/v1/enterprises', headers)
            .then((res) => {
                dispatch(listEnterprises(res.data))
                dispatch(stopLoading())
            }
            ).catch((error) => {
                alert('it was not possible to load the list of enterprises')
                console.log(error);
            })
    }
}

export const filteredEnterprises = (type, name) => {
    return async dispatch => {
        dispatch(loadingFilter())
        let headers = await getHeadersFromStorage()
        let urlFilter = 'https://empresas.ioasys.com.br/api/v1/enterprises?' + 'enterprise_types=' + type.toString() + '&' + 'name=' + name
        axios.get(urlFilter, headers)
            .then((res) => {
                dispatch(filterEnterprises(res.data))
                dispatch(stopLoadingFilter())
                if(res.data.enterprises.length===0){
                    alert('Enterprises not found')
                }
            }).catch((error) => {
                alert('Error loading data')
                console.log(error);
            })
    }
}