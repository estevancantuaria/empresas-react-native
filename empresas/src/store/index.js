import { configureStore } from '@reduxjs/toolkit';

import rootEnterprises from './ducks/enterprises';
import rootAuth from './ducks/auth';
import rootFilterEnterprises from './ducks/filteredEnterprises'

export default configureStore({
  reducer: {
    enterprises: rootEnterprises,
    auth: rootAuth,
    filteredEnterprises: rootFilterEnterprises
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
})