import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';

import { Container, ButtonMenu, ButtonSearch } from './styles';

export default function Header() {
  const navigation = useNavigation();

  return (
    <Container>
      <ButtonMenu onPress={() => navigation.toggleDrawer()}>
        <Icon name="menu" color="black" size={30} />
      </ButtonMenu>
      <ButtonSearch onPress={() => navigation.navigate('Search')}>
        <Icon name="search" color="black" size={30} />
      </ButtonSearch>
    </Container>
  );
}