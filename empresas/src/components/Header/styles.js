import styled from 'styled-components/native';

export const Container = styled.View`
    justify-content: space-between;
    flex-direction: row;
    align-items: flex-start;
    margin-top: 10px;
    padding-right: 12px;
    padding-left: 12px;
    margin-bottom: 15px;
    width: 100%;
    height: 30px;
`;

export const ButtonMenu = styled.TouchableWithoutFeedback`
    justify-content: center;
    align-items: center;
`;

export const ButtonSearch = styled.TouchableWithoutFeedback`
    justify-content: center;
    align-items: center;
`;


