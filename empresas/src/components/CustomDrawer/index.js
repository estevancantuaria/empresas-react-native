import React, { useContext } from 'react';
import { View, Text, Image } from 'react-native';
import { DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { useDispatch } from 'react-redux';
import { signOut } from '../../store/fetchActions'
export default function CustomDrawer(props) {
    const dispath = useDispatch();

    function logout() {
        dispath(signOut())
    }

    return (
        <DrawerContentScrollView  {...props} >
            <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 25 }}>
                <Image
                    source={require('../../assets/logo_ioasys.png')}
                    style={{ width: 85, height: 85 }}
                    resizeMode="contain"
                />

                <Text style={{ color: 'black', fontSize: 18, marginBottom: 14 }}>
                    Welcome
                </Text>
            </View>

            <DrawerItemList {...props} />

            <DrawerItem
                {...props}
                label="Logout"
                inactiveBackgroundColor="black"
                onPress={logout}
                activeTintColor='white'
                inactiveTintColor='white'
            />

        </DrawerContentScrollView>
    );
}