import React from 'react';
import { View, Text, Image, FlatList, TouchableOpacity } from 'react-native';
import styles from './styles'

export default function Card({ data }) {

  let apiURL = 'https://empresas.ioasys.com.br'

  const image = apiURL.concat(data.photo)

  return (
    <View styles={styles.card}>
      <Text style={styles.titulo}>{data.enterprise_name}</Text>
      <Image source={{ uri: image }} style={styles.capa} />
      <View style={styles.areaBotao}>
        <TouchableOpacity style={styles.botao}>
          <Text style={styles.botaoTexto}>Read more...</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}