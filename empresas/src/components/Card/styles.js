import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  card: {
    shadowColor: '#000',
    backgroundColor: '#FFF',
    textShadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    margin: 15,
    shadowRadius: 5,
    borderRadius: 5,
    elevation: 3,
  },
  tituloTela: {
    fontSize: 23,
    padding: 15,
    fontWeight: 'bold'
  },
  titulo: {
    fontSize: 18,
    padding: 15,
    fontWeight: 'bold'
  },
  capa: {
    height: 250,
    width: 412,
    zIndex: 2
  },
  areaBotao: {
    alignItems: 'flex-end',
    marginTop: -45,
    zIndex: 9
  },
  botao: {
    width: 110,
    backgroundColor: 'black',
    opacity: 1,
    padding: 12,
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5
  },
  botaoTexto: {
    textAlign: 'center',
    color: '#FFF'
  }
})

export default styles