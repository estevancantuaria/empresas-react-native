import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import CustomDrawer from '../components/CustomDrawer';
import Home from '../pages/Home';
import SearchScreen from '../pages/SearchScreen';



const AppDrawer = createDrawerNavigator()

function AppRoutes() {
    return (
        <AppDrawer.Navigator
            drawerContent={(props) => <CustomDrawer {...props} />}
            drawerStyle={{
                backgroundColor: 'white'
            }}
            drawerContentOptions={{
                labelStyle: {
                    fontWeight: 'bold'
                },
                activeTintColor: 'white',
                activeBackgroundColor: 'black',
                inactiveBackgroundColor: 'black',
                inactiveTintColor: '#DDD',
                itemStyle: {
                    marginVertical: 5,
                }
            }}
        >
            <AppDrawer.Screen name='Home' component={Home} />
            <AppDrawer.Screen name='Search' component={SearchScreen} />
        </AppDrawer.Navigator>
    )
}

export default AppRoutes;