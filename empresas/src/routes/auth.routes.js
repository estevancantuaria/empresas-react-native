import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'

import Signin from '../pages/Signin';
import Home from '../pages/Home';

const AuthStack = createStackNavigator()

function AuthRoutes() {
    return (
        <AuthStack.Navigator>
            <AuthStack.Screen name='Signin' component={Signin} options={{ headerShown: false }} />
        </AuthStack.Navigator>
    )
}

export default AuthRoutes;