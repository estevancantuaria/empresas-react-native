import React, { useEffect } from 'react'
import AuthRoutes from './auth.routes'
import AppRoutes from './app.routes'
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { login } from "../store/ducks/auth";
import AsyncStorage from '@react-native-community/async-storage';


function Routes() {

    const { isAuthenticated } = useSelector((state) => state.auth);


    return (
        isAuthenticated ? <AppRoutes /> : <AuthRoutes />
    )
}

export default Routes;