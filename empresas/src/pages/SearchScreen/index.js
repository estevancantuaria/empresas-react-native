import React, { useState } from 'react';
import { View, FlatList, ActivityIndicator } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Header from '../../components/Header';
import { AreaInput, Input, ContainerInput, SubmitButton, SubmitText } from './styles'
import { enterpriseTypes } from '../../utils/enterpriseTypes'
import { useDispatch, useSelector } from 'react-redux';
import { filteredEnterprises } from '../../store/fetchActions'
import Card from '../../components/Card';


export default function SearchScreen() {

    const [enterType, setEnterType] = useState('')
    const [enterName, setEnterName] = useState('')

    const types = enterpriseTypes;

    const { isLoading, enterprises } = useSelector((state) => state.filteredEnterprises)

    const dispath = useDispatch();

    function submit() {
        dispath(filteredEnterprises(enterType, enterName))
    }

    return (
        <View style={{ flex: 1 }} >
            <Header />
            <ContainerInput>
                <AreaInput>
                    <Input
                        placeholder='Set enterprise name'
                        autocorrect={false}
                        autoCapitalize='none'
                        value={enterName}
                        onChangeText={setEnterName}
                    />
                    <Picker
                        selectedValue={enterType}
                        onValueChange={(itemValue, itemIndex) => {
                            setEnterType(itemValue)
                        }
                        }
                        accessibilityLabel="Styled Picker Accessibility Label"
                        dropdownIconColor="black"
                    >
                        {types.map((obj) =>
                            <Picker.Item key={obj.id} label={obj.enterpriseTypeName} value={obj.id} style={{ backgroundColor: 'rgb(236, 234, 234)', color: 'black', fontSize: 17 }} />
                        )}
                    </Picker>
                    <SubmitButton onPress={submit}>
                        <SubmitText>Search</SubmitText>
                    </SubmitButton>
                </AreaInput>
            </ContainerInput>
            {
                !isLoading ?
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={enterprises['enterprises']}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => (<Card data={item} />)}

                    /> :
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <ActivityIndicator color='black' size={40} />
                    </View>
            }

        </View>
    );
}
