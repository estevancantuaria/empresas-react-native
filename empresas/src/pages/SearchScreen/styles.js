import styled from 'styled-components'

export const AreaInput = styled.View`
flex-direction: column;
padding-left: 10px;
padding-right: 10px;
`
export const Input = styled.TextInput.attrs({
    placeholderTextColor: 'black'
})`
background: rgb(236, 234, 234);
width: 100%;
font-size: 17px;
color:black;
margin-bottom: 17px;
padding: 10px;
border-radius: 7px;
`
export const ContainerInput = styled.View`
    margin-top: 12px;
`
export const SubmitButton = styled.TouchableOpacity`
    align-items: center;
    justify-content: center;
    background-color: black;
    width: 100%;
    height: 42px;
    border-radius: 7px;
    margin-top: 17px;
`
export const SubmitText = styled.Text`
    font-size: 19px;
    color: white
`