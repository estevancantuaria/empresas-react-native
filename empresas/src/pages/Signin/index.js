import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { authLogin } from '../../store/fetchActions'
import {
  Background,
  Container,
  Logo,
  AreaInput,
  Input,
  SubmitButton,
  SubmitText,
} from './styles'
import { View, ActivityIndicator } from 'react-native'


export default function Signin() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispath = useDispatch();

  const { isLoading } = useSelector((state) => state.auth);

  function submit() {
    dispath(authLogin(email, password))
  }

  return (
    <Background>
      <Container
        enabled
      >
        <Logo source={require('../../assets/logo_ioasys.png')} />

        <AreaInput>
          <Input
            placeholder='Email'
            autocorrect={false}
            autoCapitalize='none'
            value={email}
            onChangeText={setEmail}
          />
        </AreaInput>

        <AreaInput>
          <Input
            placeholder='Password'
            autocorrect={false}
            autoCapitalize='none'
            value={password}
            secureTextEntry={true}
            onChangeText={setPassword}
          />
        </AreaInput>


        <SubmitButton onPress={submit}>
          {
            isLoading ?
              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                <ActivityIndicator color='white' size={40} />
              </View>
              :
              <SubmitText>Log In</SubmitText>
          }

        </SubmitButton>

      </Container>
    </Background>
  );
}



