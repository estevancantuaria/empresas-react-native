import styled from 'styled-components'


export const Background = styled.View`
flex:1;
background-color: white
`

export const Container = styled.KeyboardAvoidingView`
flex:1;
align-items: center;
justify-content: center;
`

export const Logo = styled.Image`
margin-bottom: 14px;
`

export const AreaInput = styled.View`
flex-direction: row;
`

export const Input = styled.TextInput.attrs({
    placeholderTextColor: 'black'
})`
background: rgba(0,0,0,0.20);
width: 90%;
font-size: 17px;
color:black;
margin-bottom: 17px;
padding: 10px;
border-radius: 7px;
`

export const SubmitButton = styled.TouchableOpacity`
    align-items: center;
    justify-content: center;
    background-color: black;
    width: 90%;
    height: 45px;
    border-radius: 7px;
    margin-top: 13px;
`
export const SubmitText = styled.Text`
    font-size: 20px;
    color: white
`

