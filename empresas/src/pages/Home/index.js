import React, { useEffect } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import Header from '../../components/Header';
import styles from './styles'
import { enterprisesList } from '../../store/fetchActions'
import { useDispatch, useSelector } from 'react-redux';
import Card from '../../components/Card';

export default function Home() {

  const enterprises = useSelector((state) => state.enterprises)

  const dispatch = useDispatch();

  useEffect(
    () => {
      dispatch(enterprisesList());
    },
    []
  );

  return (
    <View>
      <Header />
      <Text style={styles.tituloTela}>Ioasys Partners</Text>
      {
        enterprises["enterprises"] ?
          <FlatList
            showsVerticalScrollIndicator={false}
            data={enterprises["enterprises"]}
            keyExtractor={item => item.id}
            renderItem={({ item }) => (<Card data={item} />)}
          /> :
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <ActivityIndicator color='black' size={40} />
          </View>
      }

    </View>
  );
}
