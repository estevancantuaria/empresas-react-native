import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({

    tituloTela: {
        fontSize: 23,
        padding: 15,
        fontWeight: 'bold'
    },

})

export default styles